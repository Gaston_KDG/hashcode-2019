package be.hashcode;


import java.util.*;

public class Photo {
    private final boolean vertical;
    private final List<String> tags;
    private final long id;
    private Set<Photo> photoLink;

    public Set<Photo> getPhotoLink() {
        return photoLink;
    }

    public Photo(long id, String orientation, List<String> tags) {
        this.vertical = orientation.equalsIgnoreCase("V");
        this.tags = tags;
        this.id = id;
        photoLink = new HashSet<>();
    }

    public boolean isVertical() {
        return vertical;
    }

    public List<String> getTags() {
        return tags;
    }

    public long getId() {
        return id;
    }

    public void addPhoto(Photo photo) {
        List<String> otherTags = photo.getTags();
        for (String tag : otherTags) {
            if (tags.contains(tag)) {
                photoLink.add(photo);
                photo.photoLink.add(this);
                return;
            }
        }
    }


    public int calcInterestFactor(Photo other) {
        return (int) Helper.calcInterestFactor(tags,other.tags);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Photo photo = (Photo) o;

        return id == photo.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Photo=  " + id + " - " + vertical + " - " + tags;
    }
}
