package be.hashcode;

import java.util.HashSet;
import java.util.Set;

public class Slide {
    Photo photo;
    Photo photo2;

    Set<String> tagsRight = new HashSet<>();
    Set<String> tagsLeft = new HashSet<>();

    public int calcInterestFactor(Slide other) {
        return (int) Helper.calcInterestFactor(tagsLeft, other.tagsLeft);
    }

    public Slide(Photo photo) {
        this.photo = photo;
        this.tagsLeft.addAll(photo.getTags());
        this.tagsRight.addAll(photo.getTags());
    }

    public Slide(Photo photo, Photo photo2) {
        this.photo = photo;
        this.photo2 = photo2;
        this.tagsLeft.addAll(photo.getTags());
        this.tagsRight.addAll(photo2.getTags());
    }

    public Set<String> getTagsLeft() {
        return tagsLeft;
    }

    public Set<String> getTagsRight() {
        return tagsRight;
    }

    @Override
    public String toString() {
        String formattedString = String.format("%d", photo.getId());
        if (photo2 != null) {
            formattedString = formattedString + String.format(" %d", photo2.getId());
        }
        return formattedString;
    }
}
