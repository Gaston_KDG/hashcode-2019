package be.hashcode;

import be.gato.io.IOFactory;
import be.hashcode.common.NotImplementedException;
import be.hashcode.common.Performance;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class Main {
    public static void main(String[] args) {
        Performance.run(Main::runSetA, "Example Completed in");
        Performance.run(Main::runSetB, "Lovely Landscapes Completed in");
        Performance.run(Main::runSetC, "Memorable moments Completed in");
        Performance.run(Main::runSetD, "Pet Pictures Completed in");
        Performance.run(Main::runSetE, "Shiny selfies Completed in");
    }

    private static List<Slide> doStuff(List<Photo> photos) {
        Collections.shuffle(photos);
        List<Photo> verticals = photos.stream()
                .filter(Photo::isVertical)
                .collect(toList());

        if (verticals.size() % 2 != 0) {
            verticals.remove(0);
        }

        List<Slide> slides = photos.stream()
                .filter(photo -> !photo.isVertical())
                .map(Slide::new)
                .collect(toList());

        slides.addAll(divideAndConquerVerticalPhotos(verticals));

        System.out.println("all slides " + slides.size());
        return divideAndConquerSlides(slides);
    }

    private static List<Slide> divideAndConquerVerticalPhotos(List<Photo> verticals) {
        List<List<Photo>> monster = new ArrayList<>();

        int threshold = 1000;
        if (verticals.size() < threshold) monster.addAll(Collections.singleton(verticals));
        for (int i = threshold; i < verticals.size(); i += threshold) {
            if (i == 0) {
                monster.add(verticals.subList(0, threshold));//0-500
            } else {
                int rest = i % threshold;
                if (rest == 0 && (i + threshold) <= verticals.size()) {
                    monster.add(verticals.subList(i, i + threshold));//500-1000
                } else monster.add(verticals.subList(i, i + rest));
            }
        }
        return monster.stream()
                .parallel()
                .map(Main::sortVerticals)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private static List<Slide> sortVerticals(List<Photo> verticals) {
        LinkedList<Slide> linked = new LinkedList<>();
        List<Photo> photos = new ArrayList<>(verticals);

        for (int i = 0; i < photos.size(); i++) {
            Photo photo = photos.get(i);
            var found = searchForLeastMatch(photo, photos.subList(i + 1, photos.size()));
            linked.add(new Slide(photo, found));
            photos.remove(found);
        }

        return linked;
    }

    private static Photo searchForLeastMatch(Photo photo, List<Photo> subList) {
        Optional<Photo> optionalPhoto = subList.stream()
                .filter(p -> !p.getTags().containsAll(photo.getTags()))
                .findFirst();
        if (optionalPhoto.isPresent()) return optionalPhoto.get();
        else {
            var r = new Random();
            return subList.get(r.nextInt(subList.size()));
        }
    }

    private static List<Slide> divideAndConquerSlides(List<Slide> slides) {
        List<List<Slide>> monster = new ArrayList<>();

        int threshold = 1000;
        if (slides.size() < threshold) monster.addAll(Collections.singleton(slides));
        for (int i = threshold; i < slides.size(); i += threshold) {
            if (i == 0) {
                monster.add(slides.subList(0, threshold));//0-500
            } else {
                int rest = i % threshold;
                if (rest == 0 && (i + threshold) <= slides.size()) {
                    monster.add(slides.subList(i, i + threshold));//500-1000
                } else monster.add(slides.subList(i, i + rest));
            }
        }
        return monster.stream()
                .parallel()
                .map(Main::sortSlides)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private static List<Slide> sortSlides(List<Slide> slides) {
        LinkedList<Slide> linked = new LinkedList<>();
        List<Slide> newslides = new ArrayList<>(slides);
        List<Slide> stuff = new ArrayList<>();

        for (int i = 0; i < newslides.size(); i++) {
            if (linked.isEmpty()) linked.add(newslides.get(i));
//            if (i % 1000 == 0) System.out.println(i);
            if (linked.size() == 1) {
                Slide foundLeft = searchForMatchLeft(linked.getFirst(), newslides.subList(i + 1, newslides.size()));
                Slide foundRight = searchForMatchRight(linked.getFirst(), newslides.subList(i + 1, newslides.size()));
                if (foundLeft != null) {
                    linked.addFirst(foundLeft);
                    newslides.remove(foundLeft);
                }
                if (foundRight != null && foundLeft != foundRight) {
                    linked.addLast(foundRight);
                    newslides.remove(foundRight);
                } else {
                    newslides.remove(linked.getFirst());
                    stuff.add(linked.getFirst());
                }
            } else {
                var first = linked.getFirst();
                var last = linked.getLast();
                Slide foundLeft = searchForMatchLeft(first, newslides.subList(i + 1, newslides.size()));
                Slide foundRight = searchForMatchRight(last, newslides.subList(i + 1, newslides.size()));
                if (foundLeft != null) {
                    linked.addFirst(foundLeft);
                    newslides.remove(foundLeft);
                }
                if (foundRight != null && foundLeft != foundRight) {
                    linked.addLast(foundRight);
                    newslides.remove(foundRight);
                }
                stuff.add(newslides.get(i));
            }
        }
        Collections.shuffle(stuff);
        for (Slide slide : stuff) {
            if (!linked.contains(slide)) linked.add(slide);
        }
        return linked;
    }

    private static Slide searchForMatchRight(Slide slide, List<Slide> subList) {
        for (Slide s : subList) {
            if (slide.getTagsRight().stream()
                    .anyMatch(tag -> s.getTagsLeft().contains(tag)))
                return s;
        }
        return null;
    }

    private static Slide searchForMatchLeft(Slide slide, List<Slide> subList) {
        for (Slide s : subList) {
            if (slide.getTagsLeft().stream()
                    .anyMatch(tag -> s.getTagsRight().contains(tag)))
                return s;
        }
        return null;
    }


    private static void runSetA() {
        List<Photo> photos = loadSet("a_example.txt");
        List<Slide> slides = doStuff(photos);
        writeSet("a.out", slides);
    }

    private static void runSetB() {
        List<Photo> photos = loadSet("b_lovely_landscapes.txt");
        List<Slide> slides = doStuff(photos);

        writeSet("b.out", slides);
    }

    private static void runSetC() {
        List<Photo> photos = loadSet("c_memorable_moments.txt");
        List<Slide> slides = doStuff(photos);

        writeSet("c.out", slides);
    }

    private static void runSetD() {
        List<Photo> photos = loadSet("d_pet_pictures.txt");
        List<Slide> slides = doStuff(photos);

        writeSet("d.out", slides);
    }

    private static void runSetE() {
        List<Photo> photos = loadSet("e_shiny_selfies.txt");
        List<Slide> slides = doStuff(photos);

        writeSet("e.out", slides);
    }


    private static List<Photo> loadSet(String name) {
        try {
            var blr = IOFactory.createBulkLinesReader(Paths.get(Objects.requireNonNull(Main.class.getClassLoader().getResource(name)).toURI()));
            var strs = blr.read();
            List<Photo> photos = new ArrayList<>();
            long total = Long.parseLong(strs.get(0));
            //skip the first one
            for (int i = 1; i <= total; i++) {
                String[] variables = strs.get(i).split(" ");
                int amountOfTags = Integer.parseInt(variables[1]);
                String[] tagArray = new String[amountOfTags];
                System.arraycopy(variables, 2, tagArray, 0, amountOfTags);
                List<String> tags = Arrays.asList(tagArray);
                photos.add(new Photo((i - 1), variables[0], tags));
            }
            return photos;
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new NotImplementedException();
    }

    private static void writeSet(String name, List<Slide> slides) {
        System.out.println("writing " + slides.size());
        try {
            var appender = IOFactory.createLineAppender(name, true);
            appender.append(slides.size());
            appender.appendLines(slides);
            appender.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
