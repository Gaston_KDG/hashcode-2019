package be.hashcode;

import java.util.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Helper {
    private Helper() {
    }

    public static Map<String, Integer> countTags(List<Photo> photos) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        for (Photo photo : photos) {
            for (String tag : photo.getTags()) {
                hashMap.putIfAbsent(tag, 0);
                if (!hashMap.get(tag).equals(0)) {
                    hashMap.put(tag, hashMap.get(tag) + 1);
                }
            }
        }
        return hashMap;
    }

    public static List<Slide> calculate(List<Photo> photos) {
        ArrayList<Slide> slides = new ArrayList<>();
        Photo photo = sortPhotos(photos);
        do {
            deletePhoto(photo);
            slides.add(new Slide(photo));
            photo = sortPhotos(photos);
        } while (photo.getPhotoLink().size() > 0);
        return slides;
    }

    static Photo sortPhotos(List<Photo> photos) {
        return photos.stream()
                .sorted(Comparator.comparingInt(o -> o.getPhotoLink().size()))
                .findFirst().get();
    }

    static void deletePhoto(Photo photo) {
        photo.getPhotoLink().forEach(linkedPhoto -> linkedPhoto.getPhotoLink().remove(photo));
    }


    public static long calcInterestFactor(Collection<String> a, Collection<String> b) {
        long common = a.stream().filter(b::contains).count();
        long aNOTb = a.stream().filter(t -> !b.contains(t)).count();
        long bNOTa = b.stream().filter(t -> !a.contains(t)).count();
        return Math.min(Math.min(aNOTb, bNOTa), common);
    }

}
